﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 1

Problemas propuestos para la Sesión 1 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Los **objetivos de la práctica** son:

- Definir adecuadamente las clases que se piden en la práctica.
- Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.

Los ejercicios son diferentes para cada grupo:
- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion1/-/blob/master/README.md#grupo-6)

### Grupo 1
En un sistema distribuido el gestor de memoria deberá mantener una carga equilibrada entre los diferentes procesos que se creen y de la disponibilidad de los ordenadores presentes en un momento determinado en el sistema.

Hay que definir las siguientes clases en el ejercicio:

 - `Proceso` : Tiene que tener un identificador único para cada proceso del sistema y además tendrá asociado el `TipoProceso` que determinará el espacio que ocupa en memoria. Se tiene que definir el constructor, los métodos de acceso y el método `toString()` para dar una representación al proceso cuando se muestre por la consola.
 - `Ordenador` : Tiene un identificador para diferenciarlo dentro del sistema. Una capacidad máxima para cada `TipoProceso` dentro de su memoria.
	 - Definir el constructor de la clase.
	 - Hay que definir los métodos de acceso correspondientes.
	 - Un método que permite incluir un nuevo proceso al ordenador, tiene que devolver el número de procesos del mismo tipo que aún puede almacenar o `MEMORIA_COMPLETA` si no ha podido almacenar el proceso en su memoria.
	-  Definir el método `toString()` para mostrar los procesos de cada tipo que tiene almacenados en su memoria.

- `HiloPrincipal` :
	- Crear un número variable de ordenadores que compondrán el sistema. Este valor estará comprendido entre 2 y 5 ordenadores. La capacidad que tiene cada ordenador también es aleatoria para cada tipo de proceso. El número de procesos de cada tipo también estará comprendido entre 2 y 5.
	- Crear hasta 30 procesos, el tipo de proceso será generado de forma aleatoria. Cada vez que se cree un proceso se tendrá que asignar a un ordenador. La política de asignación deberá mantener una carga equilibrada de los diferentes tipos de procesos entre los ordenadores disponibles en el sistema.
	- Si no se ha encontrado ordenador disponible para el proceso de deberá anotar para que al final de la ejecución se indiquen los procesos que no se han podido almacenar en el sistema.
	- Una vez que se ha completado la creación de los diferentes procesos se mostrará por consola el estado del almacenamiento del sistema.

### Grupo 2
Disponemos de un sistema cuya política de planificación de procesos es mediante colas de prioridad múltiple.

En el ejercicio debemos desarrollar las siguientes clases:

 - `Proceso` : Tiene que tener un identificador único en el sistema y además un `TipoPrioridad` para poder añadirlo apropiadamente a la cola correspondiente. Se tiene que definir el constructor, los métodos de acceso y el método `toString()` para dar una representación al proceso cuando se muestre por la consola.
 - `ColaPrioridad` : Tiene un identificador como variable de instancia y además tiene una capacidad máxima para cada `TipoPrioridad` presente en el sistema. 
	 - Definir el constructor de la clase.
	 - Hay que definir los métodos de acceso correspondientes.
	 - Un método que permite incluir un nuevo proceso a la cola, si se ha alcanzado el máximo en su prioridad se añade a la prioridad inmediatamente inferior si hay espacio. En caso de no poder asignar el proceso a ninguna prioridad se deberá devolver `NO_ASIGNADO`.
	-  Definir el método `toString()` para mostrar los procesos almacenados en las diferentes prioridades.

- `HiloPrincipal` :
	- Crear un número variable de colas de prioridad que compondrán el sistema. Este valor estará comprendido entre 2 y 5 colas diferentes. La capacidad disponible para cada prioridad también es aleatoria, estará comprendido entre 2 y 5.
	- Crear hasta 30 procesos, el tipo de prioridad será generado de forma aleatoria. Cada vez que se cree un proceso se tendrá que asignado a una cola de prioridad. La política de asignación deberá mantener una carga equilibrada de los diferentes procesos entre las colas y que pueda garantizar que se respeta su prioridad si es posible.
	- Si no ha podido ser asignado a ninguna cola se debe anotar y comunicarlo a la finalización de la ejecución.
	- Una vez que se ha completado la creación de los diferentes procesos se mostrará por consola el estado de las colas del sistema.

### Grupo 3
Disponemos de un sistema de almacenamiento con diferentes unidades y capacidades.

En el ejercicio se tienen que definir las siguientes clases:

 - `Archivo` : Tiene que tener un identificador único en el sistema y además un `TipoArchivo` asociado al tamaño que ocupara en la unidad de almacenamiento. Se tiene que definir el constructor, los métodos de acceso y el método `toString()` para dar una representación al archivo cuando se muestre por la consola.
 - `UnidadAlmacenamiento` : Tiene un identificador único en el sistema y una capacidad máxima de almacenamiento en unidades.
	 - Definir el constructor de la clase.
	 - Hay que definir los métodos de acceso correspondientes.
	 - Un método que permite incluir un nuevo archivo a la unidad. En caso de no poder almacenar el archivo se deberá devolver `ESPACIO_INSUFICIENTE`.
	-  Definir el método `toString()` para mostrar los archivos almacenados en el disco así como su espacio disponible.
- `HiloPrincipal` :
	- Crear un número variable de unidades de almacenamiento en el sistema. Este valor estará comprendido entre 2 y 5 unidades de almacenamiento. La capacidad disponible para cada unidad de almacenamiento es aleatoria, estará comprendido entre 20 y 50 unidades de almacenamiento.
	- Crear hasta 30 Archivos, el tipo de cada uno de ellos será aleatorio entre los tipos disponibles. Cada vez que se cree un proceso se asignará a una unidad de almacenamiento. La política de asignación deberá localizar la primera unidad de almacenamiento disponible, desde la última asignación, y si no encuentra ninguna deberá anotarse.
	- Si no ha podido ser asignado a ninguna unidad de almacenamiento se comunicará a la finalización de la ejecución.
	- Una vez que se ha completado la creación de los diferentes archivos se mostrará por consola el estado de las unidades de almacenamiento del sistema.

### Grupo 4
En un sistema de reparto de comida a domicilio se agrupan los platos cocinados en menús/pedidos compuestos por un principal, un segundo y un postre. 

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
- `Plato`: Identificará a un plato por medio de un número de identificación. También tendrá asociado un `TipoPlato` que identifica si es un principal, un segundo o un postre. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
- `MenuReparto`: Identificará a un pedido por medio de un número de identificación. Debe tener un plato de cada uno de los tipos de  `TipoPlato` para considerar que el pedido está completo.
  - Hay que definir el constructor y los métodos de acceso correspondientes.
  - Definir el método `toString()` para mostrar los elementos del menú con los datos de cada plato. Si le falta algún componente por asignar debe indicar que está incompleto.
- `Hilo Principal`:
  - Generar una lista de 10 menús para reparto.
  - Generar 20 platos:
    - Generar un valor de construcción aleatorio entre 0 y 100.
    - Seleccionar el tipo de plato correspondiente para el plato con el valor de construcción generado.
  - Asignar cada plato a uno de los pedidos que aún no tenga ese tipo de plato.
  - Presentar los menús de la lista de pedidos.

### Grupo 5
En una granja de impresión 3D se tienen una serie de impresoras, pero cada impresora solo puede imprimir con una calidad predeterminada, médica, taller o industrial.  

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
- `Modelo`: Identificará a un modelo listo para imprimir por medio de un número de identificación. También tendrá asociado un `CalidadImpresión` que identifica la impresora necesaria. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
- `Impresora3D`: Identificará a una impresora 3D por medio de un número de identificación y contará con una propiedad que indique su calidad de impresión. Solo se le podrán asignar trabajos de impresión que coincidan con la calidad asignada.
  - Hay que definir el constructor y los métodos de acceso correspondientes.
  - Definir el método `toString()` para mostrar los datos de la impresora y la cola de impresión con los datos de cada modelo. 
- `Hilo Principal`:
  - Generar una lista de 10 impresoras con una calidad de impresión aleatoria.
  - Generar 20 modelos:
    - Generar un valor de construcción aleatorio entre 0 y 100.
    - Seleccionar la calidad de impresión correspondiente para el modelo con el valor de construcción generado.
  - Asignar cada modelo a una de las impresoras que acepte la calidad requerida del modelo.
  - Presentar las impresoras de la granja de impresión.

### Grupo 6
En una clínica se han recibido dosis para realizar la vacunación contra el virus del COBIS-35. Se han recibido dosis de tres laboratorios distintos, Pfinos, Antigua y AstroLunar, a cada paciente se le tienen que administrar dos dosis de la vacuna, pero no se pueden mezclar.  

Para la realización del ejercicio propuesto se deben crear las siguientes clases:
- `DosisVacuna`: Identificará a cada dosis individual por medio de un número de identificación. También tendrá asociado un `FabricanteVacuna` que identifica a su productor. Se tiene que definir el constructor, los métodos de acceso y el método `toString()`.
- `Paciente`: Identificará a cada paciente por medio de un número de indentificación, para salvaguardar su identidad real y contará con una propiedad que indique que tipo de vacuna ha recibido cuando se le asigne la primera dosis. Si no ha recibido ninguna dosis, cualquier vacuna es válida y cuando vaya a recibir la segunda dosis, solo se le podrá administrar una del mismo laboratorio.
  - Hay que definir el constructor y los métodos de acceso correspondientes.
  - Definir el método `toString()` para mostrar los datos del paciente y las dosis recibidas. También se indicará si el paciente es immune al recibir la segunda dosis. 
- `Hilo Principal`:
  - Generar una lista de 10 pacientes.
  - Generar 20 dosis de vacunas:
    - Generar un valor de construcción aleatorio entre 0 y 100.
    - Seleccionar el fabricante correspondiente para la dosis con el valor de construcción generado.
  - Asignar las dosis a los pacientes siguiendo las reglas anteriores. Como mucho dos dosis por paciente y del mismo tipo.
  - Imprimir los detalles de los pacientes.

