/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion1.grupo6.Utils.DOSIS_A_GENERAR;
import es.uja.ssccdd.curso2021.problemassesion1.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo6.Utils.PACIENTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo6.Utils.VALOR_GENERACION;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {

    public static void main(String[] args) {

        // Variables aplicación
        ArrayList<Paciente> listaPacientes;
        ArrayList<DosisVacuna> listaDosisVacunas;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos los pacientes y la lista de dosis de vacuna disponibles
        listaPacientes = new ArrayList<>();
        for (int i = 0; i < PACIENTES_A_GENERAR; i++) {
            listaPacientes.add(new Paciente(i + 1));
        }

        listaDosisVacunas = new ArrayList<>();
        for (int i = 0; i < DOSIS_A_GENERAR; i++) {
            int aleatorioFabricante = Utils.random.nextInt(VALOR_GENERACION);
            listaDosisVacunas.add(new DosisVacuna(i + 1, FabricanteVacuna.getFabricante(aleatorioFabricante)));
        }

        // Cuerpo principal de ejecución
        int i = 0;
        while (i < DOSIS_A_GENERAR) {

            int j = 0;
            boolean inyectada = false;
            while (j < PACIENTES_A_GENERAR && !inyectada) {

                if (listaPacientes.get(j).addDosisVacuna(listaDosisVacunas.get(i))) {
                    inyectada = true;
                }

                j++;
            }

            if (!inyectada) {
                System.out.println("La dosis (" + listaDosisVacunas.get(i) + ") no ha podido asignarse a ningún paciente.");
            }

            i++;
        }

        // Mostrar la información de la lista de pedidos
        System.out.println("(HILO_PRINCIPAL) Lista de pacientes");
        for (Paciente paciente : listaPacientes) {
            System.out.println(paciente);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}
