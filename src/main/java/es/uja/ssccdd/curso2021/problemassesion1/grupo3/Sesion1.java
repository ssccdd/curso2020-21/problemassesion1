/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.MAXIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.MINIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.NUM_ARCHIVOS;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.TipoArchivo.getTipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo3.Constantes.aleatorio;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        int iD = 0;
        Archivo archivo;
        ArrayList<Archivo> noAsignado;
        int totalUnidadesAlmacenamiento;
        UnidadAlmacenamiento[] listaUnidadAlmacenamiento;
        int capacidad; // Capacidad de almacenamiento para las unidades de almacenamiento
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las unidades de almacenamiento del sistema
        totalUnidadesAlmacenamiento = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaUnidadAlmacenamiento = new UnidadAlmacenamiento[totalUnidadesAlmacenamiento];
        for(int i = 0; i < listaUnidadAlmacenamiento.length; i++) {
            capacidad = MINIMO_ALMACENAMIENTO + aleatorio.nextInt(MAXIMO_ALMACENAMIENTO - 
                                                                  MINIMO_ALMACENAMIENTO + 1);
            listaUnidadAlmacenamiento[i] = new UnidadAlmacenamiento(i,capacidad);
        }
        
        // Cuerpo principal de ejecución
        noAsignado = new ArrayList();
        int ultimaUnidad = 0; // 
        for(int i = 0 ; i < NUM_ARCHIVOS; i++) {
            archivo = new Archivo(iD, getTipoArchivo(aleatorio.nextInt(VALOR_CONSTRUCCION)));
            iD++;
                    
            // Buscamos una cola con prioridad para asignar el proceso
            boolean asignado = false;
            int unidadesRevisadas = 0;
            while( (unidadesRevisadas < listaUnidadAlmacenamiento.length) && !asignado ) {
                int indice = (ultimaUnidad + unidadesRevisadas) % listaUnidadAlmacenamiento.length;
                if(listaUnidadAlmacenamiento[indice].addArchivo(archivo) != ESPACIO_INSUFICIENTE)
                    asignado = true;
                else
                    unidadesRevisadas++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // la última cola donde se asignó un proceso
            if(asignado)
                ultimaUnidad = (ultimaUnidad + unidadesRevisadas + 1) % listaUnidadAlmacenamiento.length;
            else 
                noAsignado.add(archivo);
        }
        
        // Mostrar el estado de sistema
        System.out.println("(HILO_PRINCIPAL) Lista de unidades de almacenamiento del sistema");
        for( UnidadAlmacenamiento unidadAlmacenamiento : listaUnidadAlmacenamiento )
            System.out.println(unidadAlmacenamiento);
        
        System.out.println("Archivos no almacenados en el sistema ");
        for( Archivo file : noAsignado)
            System.out.println("\t" + file);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
