/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo4;

import es.uja.ssccdd.curso2021.problemassesion1.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo4.Utils.MENUS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo4.Utils.PLATOS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo4.Utils.VALOR_GENERACION;


/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {


    public static void main(String[] args) {

        // Variables aplicación
        MenuReparto[] listaPedidos;
        Plato[] listaPlatos;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos los menús y la lista de platos para los pedidos
        listaPedidos = new MenuReparto[MENUS_A_GENERAR];
        for (int i = 0; i < MENUS_A_GENERAR; i++) {
            listaPedidos[i] = new MenuReparto(i + 1);
        }

        listaPlatos = new Plato[PLATOS_A_GENERAR];
        for (int i = 0; i < PLATOS_A_GENERAR; i++) {
            int aleatorioPlato = Utils.random.nextInt(VALOR_GENERACION);
            listaPlatos[i] = new Plato(i + 1, TipoPlato.getPlato(aleatorioPlato));
        }

        // Cuerpo principal de ejecución
        int i = 0;
        while (i < PLATOS_A_GENERAR) {
            
            int j = 0;
            boolean insertado = false;
            while( j < MENUS_A_GENERAR && !insertado){
                if(listaPedidos[j].addPlato(listaPlatos[i]))
                    insertado=true;
                j++;
            }
            
            if(!insertado)
                System.out.println("El plato " + listaPlatos[i] + " no ha podido asignarse a ningún pedido.");
            
            i++;
        }

        // Mostrar la información de la lista de pedidos
        System.out.println("(HILO_PRINCIPAL) Lista de pedidos de menús");
        for (MenuReparto pedido : listaPedidos) {
            System.out.println(pedido);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}

