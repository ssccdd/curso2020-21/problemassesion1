/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.NUM_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.TIPOS_PRIORIDAD;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.TipoPrioridad.getTipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo2.Constantes.aleatorio;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        int iD = 0;
        Proceso proceso;
        ArrayList<Proceso> noAsignado;
        int totalColasPrioridad;
        ColaPrioridad[] listaColaPrioridad;
        int[] capacidad; // Capacidad de almacenamiento para los ordenadores
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos las colas de prioridad que tiene el sistema
        totalColasPrioridad = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaColaPrioridad = new ColaPrioridad[totalColasPrioridad];
        capacidad = new int[TIPOS_PRIORIDAD]; 
        for(int i = 0; i < listaColaPrioridad.length; i++) {
            // Generamos la capacidad de la cola con prioridad
            for(int j = 0; j < TIPOS_PRIORIDAD; j++)
                capacidad[j] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
            listaColaPrioridad[i] = new ColaPrioridad(i,capacidad);
        }
        
        // Cuerpo principal de ejecución
        noAsignado = new ArrayList();
        int ultimaCola = 0; // última cola donde se asignó un proceso
        for(int i = 0 ; i < NUM_PROCESOS; i++) {
            proceso = new Proceso(iD, getTipoPrioridad(aleatorio.nextInt(VALOR_CONSTRUCCION)));
            iD++;
                    
            // Buscamos una cola con prioridad para asignar el proceso
            boolean asignado = false;
            int colasRevisadas = 0;
            while( (colasRevisadas < listaColaPrioridad.length) && !asignado ) {
                int indice = (ultimaCola + colasRevisadas) % listaColaPrioridad.length;
                if(listaColaPrioridad[indice].addProceso(proceso) != NO_ASIGNADO)
                    asignado = true;
                else
                    colasRevisadas++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // la última cola donde se asignó un proceso
            if(asignado)
                ultimaCola = (ultimaCola + colasRevisadas + 1) % listaColaPrioridad.length;
            else 
                noAsignado.add(proceso);
        }
        
        // Mostrar el estado de sistema
        System.out.println("(HILO_PRINCIPAL) Lista de colas de prioridad del sistema");
        for( ColaPrioridad colaPrioridad : listaColaPrioridad )
            System.out.println(colaPrioridad);
        
        System.out.println("Procesos no asignados a ninguna cola del sistema ");
        for( Proceso proc : noAsignado)
            System.out.println("\t" + proc);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }    
}
