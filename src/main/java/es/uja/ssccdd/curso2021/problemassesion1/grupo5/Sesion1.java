/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion1.grupo4.Utils.VALOR_GENERACION;
import es.uja.ssccdd.curso2021.problemassesion1.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo5.Utils.IMPRESORAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo5.Utils.MODELOS_A_GENERAR;
import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion1 {

    public static void main(String[] args) {

        // Variables aplicación
        ArrayList<Impresora3D> listaImpresoras;
        ArrayList<Modelo> modelosPendientes;

        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");

        // Inicializamos las impresoras y la lista de modelos
        listaImpresoras = new ArrayList<>();
        for (int i = 0; i < IMPRESORAS_A_GENERAR; i++) {
            int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
            listaImpresoras.add(new Impresora3D(i + 1, CalidadImpresion.getCalidad(aleatorioCalidad)));
        }

        modelosPendientes = new ArrayList<>();
        int siguienteImpresora = 0;

        // Cuerpo principal de ejecución
        for (int i = 0; i < MODELOS_A_GENERAR; i++) {
            int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
            Modelo modelo = new Modelo(i + 1, CalidadImpresion.getCalidad(aleatorioCalidad));
            modelosPendientes.add(modelo);

            int impresorasComprobadas = 0; //Para evitar bucles infinitos
            boolean encolado = false;
            while (!encolado && impresorasComprobadas < IMPRESORAS_A_GENERAR) {// Si no se ha generado una impresora de un tipo, esto evita el bucle infinito

                if (listaImpresoras.get(siguienteImpresora).getCalidadImpresion().equals(modelo.getCalidadRequeridad())) {
                    listaImpresoras.get(siguienteImpresora).addModelo(modelo);
                    encolado = true;
                }

                impresorasComprobadas++;
                siguienteImpresora = (siguienteImpresora + 1) % IMPRESORAS_A_GENERAR;

            }
        }

        // Mostrar la información de la lista de impresoras
        System.out.println("(HILO_PRINCIPAL) Lista de colas de impresión");
        for (Impresora3D impresora : listaImpresoras) {
            System.out.println(impresora);
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }

}
