/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo5;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();
    
    // Constantes del problema
    public static final int IMPRESORAS_A_GENERAR = 10;
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_CALIDADES = CalidadImpresion.values().length;
    public static final int MODELOS_A_GENERAR = 20;

    //Enumerado para el tipo de calidad de impresión
    public enum CalidadImpresion {
        MEDICINA(25), TALLER(75), INDUSTRIAL(100);

        private final int valor;

        private CalidadImpresion(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un calidad de impresión relacionada con su valor de
         * generación
         *
         * @param valor, entre 0 y 100, de generación de calidad
         * @return la CalidadImpresión con el valor de generación
         */
        public static CalidadImpresion getCalidad(int valor) {
            CalidadImpresion resultado = null;
            CalidadImpresion[] calidades = CalidadImpresion.values();
            int i = 0;

            while ((i < calidades.length) && (resultado == null)) {
                if (calidades[i].valor >= valor) {
                    resultado = calidades[i];
                }

                i++;
            }

            return resultado;
        }
    }
}
