/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo1;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador de números aleatorios
    public static final Random aleatorio = new Random();
    
    // Enumerado para el tipo de proceso
    public enum TipoProceso {
        PEQUEÑO(25), MEDIANO(75), GRANDE(100);
        
        private final int valor;

        private TipoProceso(int valor) {
            this.valor = valor;
        }
        
        /**
         * Obtenemos un tipo de proceso relacionado con su valor de construcción
         * @param valor, entre 0 y 100, de constucción del componente
         * @return el TipoProceso con el valor de construcción
         */
        public static TipoProceso getTipoProceso(int valor) {
            TipoProceso resultado = null;
            TipoProceso[] tipo = TipoProceso.values();
            int i = 0;
            
            while( (i < tipo.length) && (resultado == null) ) {
                if ( tipo[i].valor >= valor )
                    resultado = tipo[i];
                
                i++;
            }
            
            return resultado;
        } 
    }
    
    // Constantes del problema
    public static final int NUM_PROCESOS = 30;
    public static final int VALOR_CONSTRUCCION = 101; // Valor máximo
    public static final int TIPOS_PROCESO = TipoProceso.values().length;
    public static final int MINIMO = 2;
    public static final int MAXIMO = 5;
    public static final int MEMORIA_COMPLETA = -1;
}
