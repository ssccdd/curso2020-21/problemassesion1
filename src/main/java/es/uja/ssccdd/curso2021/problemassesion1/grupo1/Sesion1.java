/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion1.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.MEMORIA_COMPLETA;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.NUM_PROCESOS;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.TIPOS_PROCESO;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.TipoProceso.getTipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion1.grupo1.Constantes.aleatorio;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Sesion1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Variables aplicación
        int iD = 0;
        Proceso proceso;
        ArrayList<Proceso> noAsignado; 
        int totalOrdenadores;
        Ordenador[] listaOrdenadores;
        int[] capacidad; // Capacidad de almacenamiento para los ordenadores
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicializamos los ordenadores que componen el sistema distribuido
        totalOrdenadores = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaOrdenadores = new Ordenador[totalOrdenadores];
        capacidad = new int[TIPOS_PROCESO]; 
        for(int i = 0; i < listaOrdenadores.length; i++) {
            // Generamos la capacidad del ordenador
            for(int j = 0; j < TIPOS_PROCESO; j++)
                capacidad[j] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
            listaOrdenadores[i] = new Ordenador(i,capacidad);
        }
        
        
        
        // Cuerpo principal de ejecución
        noAsignado = new ArrayList();
        int ultimoOrdenador = 0; // último ordenador donde se asignó un proceso
        for(int i = 0 ; i < NUM_PROCESOS; i++) {
            proceso = new Proceso(iD, getTipoProceso(aleatorio.nextInt(VALOR_CONSTRUCCION)));
            iD++;
                    
            // Buscamos un ordenador pasa asignar el proceso
            boolean asignado = false;
            int ordenadoresRevisados = 0;
            while( (ordenadoresRevisados < listaOrdenadores.length) && !asignado ) {
                int indice = (ultimoOrdenador + ordenadoresRevisados) % listaOrdenadores.length;
                if(listaOrdenadores[indice].addProceso(proceso) != MEMORIA_COMPLETA)
                    asignado = true;
                else
                    ordenadoresRevisados++;
            }
            
            // Si no se ha asignado se anota el proceso en otro caso se actualiza
            // el último ordenador asignado
            if(asignado)
                ultimoOrdenador = (ultimoOrdenador + ordenadoresRevisados + 1) % listaOrdenadores.length;
            else 
                noAsignado.add(proceso);
        }
        
        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Lista de ordenadores del sistema distribuido");
        for( Ordenador ordenador : listaOrdenadores )
            System.out.println(ordenador);
        
        System.out.println("Procesos no asignados a ningún ordenador del sistema ");
        for( Proceso proc : noAsignado)
            System.out.println("\t" + proc);
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");

    }
    
}
